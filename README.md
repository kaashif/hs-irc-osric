hs-irc-osric
============
![Build Status](https://travis-ci.org/kaashif/hs-irc-osric.svg)

An IRC dungeon master helper bot. Current features:

* Roll some dice!
```
<osric-player123> !roll 3 4
<osric-bot> [1,1,4]
```

* Make it say things!
```
<osric-player123> !id I'm a robot
<osric-bot> I'm a robot
```

* Query the bot so it only tells you the results!
```
/query osric-bot
<osric-player123> !roll 1 6
<osric-bot> [5]
```

* Disagree on something? Put it to a vote!
```
<osric-player123> !vote osric-player234
<osric-bot> Got vote from osric-player123 for osric-player234.
<osric-player456> !vote osric-player234
<osric-bot> Got vote from osric-player456 for osric-player234.
<osric-player234> !results
<osric-bot> "osric-player234" - 2
```

* Pick a DM!
```
<osric-player123> !dm osric-player234
<osric-bot> Set DM to osric-player 234.
<osric-player123> !dm
<osric-bot> The DM is: osric-player 234.
```

Running
-------
If you don't want to pollute your system with dozens of libraries:

	$ cabal sandbox init
	$ cabal run -- -s 'irc.freenode.net' -c '#testing123'

If you do:

	$ cabal install
	$ irc-osric -s 'irc.freenode.net' -s '#testing123'

Be aware you can build in the sandbox then copy
`dist/build/irc-osric/irc-osric` to `/usr/local/bin` or wherever.

The bot creates some log files at 'easyircbot.log' and
'easyircbot-errors.log', where you'll find records of every message
the bot has received, how it interpreted the message, and how it replied.
