module EasyIRCBot ( module Control.Lens
                  , module EasyIRCBot
                  , module Control.Monad.Trans.Class
                  , module Data.List
                  , module Control.Monad.Trans.State
                  , module EasyIRCBot.Command
                  ) where
import Prelude hiding (log)
import Control.Monad.Trans.State
import Control.Monad.Trans.Class
import Control.Lens
import System.IO
import Network
import Data.List
import System.Exit
import Text.Printf
import Data.Maybe
import Data.Typeable
import Data.Either.Utils
import Data.Dynamic
import Safe
import Data.String (IsString)
import qualified Network.IRC as I
import qualified Data.ByteString.Char8 as BS
import qualified Data.Map as M
import EasyIRCBot.Logging
import EasyIRCBot.Command

-- | Convenient types
type Server = String
type Channel = String
type Nick = String
type Recipient = String

-- | Types of event that can be detected and responded to
data Command = PRIVMSG
             | PING
             | MODE
             | JOIN
             | QUIT
             | OTHER
               deriving (Show, Ord, Eq)

-- | Default boring "custom" state
data DefaultState = DefaultState String deriving Typeable


-- | Context used to send IRC messages
data IRCState a = IRCState { socket :: Handle
                           , customState :: a
                           } deriving Eq

-- | High-level IRC message type, used for final, ready-to-send messages
data Message = Message { _sender :: Nick
                       , _address :: Nick
                       , _text :: String
                       , _command :: Command
                       } deriving (Show,Eq)
makeLenses ''Message


-- | The monad used by the DSL to build up the `Bot`
type BotBuilder a = StateT (BotState a) IO

-- | The monad used to send messages given an IRC environment
type IRC a = StateT (IRCState a) IO

-- | Reasons to quit
data Reason = NoReason

-- | The bot that is built up by the DSL then run using `run`
data BotState a = BotState { _nick :: Nick
                           , _server :: Server
                           , _channel :: Channel
                           , _eventHandlers :: M.Map Command (Message -> IRC a ())
                           , _logfile :: String
                           , _errlogfile :: String
                           , _custState :: a
                           }
makeLenses ''BotState

-- | Convenience - sets a value in the BotState of a BotBuilder monad
setBotValue l v = do
  s <- get
  put $ s & l .~ v

-- | Modifies the value of an element of a BotState of a BotBuilder monad
modBotValue l f = do
  s <- get
  put $ s & l %~ f

-- | Modifies custom state of an IRC bot with given function
modCustomState :: (a -> a) -> IRC a ()
modCustomState f = do
  curIRCState <- get
  let curCustomState = customState curIRCState
  put $ curIRCState { customState = f curCustomState }

-- | Takes an event, a handler, and adds it to the bot
on :: Command -> (Message -> IRC a ()) -> BotBuilder a ()
on event handler = modBotValue eventHandlers (M.insert event handler)

-- | Sends a `Message` to someone, which may have involved impure computation to obtain
replyTo :: Recipient -> Message -> IRC a ()
replyTo r m = do
  case (m^.command) of
    PRIVMSG -> irc $ I.privmsg (BS.pack r) (BS.pack $ m^.text)
    QUIT -> irc $ I.quit Nothing
    _ -> return ()

-- | Changes the `customState` into whatever you give it
setInitialState :: a -> BotBuilder a ()
setInitialState x = setBotValue custState x

-- | Tells a bot which server to connect to
connectTo :: Server -> BotBuilder a ()
connectTo s = setBotValue server s

-- | Tells a bot which channel to join
join :: Channel -> BotBuilder a ()
join c = setBotValue channel c

-- | Tells a bot where to log normal messages
logInfoTo f = setBotValue logfile f
              
-- | Tells a bot where to log error messages
logErrorsTo f = setBotValue errlogfile f

-- | Sets a bot's nick
myNameIs s = setBotValue nick s

-- | Blank bot filled with defaults
-- | `custState` _must_ be set or left unused, or there will be
-- | a bit of a problem.
defaultBot :: BotState a
defaultBot = BotState  { _nick = "osric-bot"
                       , _server = "irc.freenode.net"
                       , _channel = "#hs-irc-osric-test"
                       , _eventHandlers = M.empty
                       , _logfile = "easyircbot.log"
                       , _errlogfile = "easyircbot-errors.log"
                       , _custState = undefined
                       }

-- | Here for convenience
startsWith :: Eq a => [a] -> [a] -> Bool
startsWith = flip isPrefixOf

-- | Decodes a low-level message and determines its command
determineMsgType :: BS.ByteString -> Command
determineMsgType = makeCommand . I.msg_command . fromJust . I.decode

-- | Decodes a low-level message and turns it into a high-level message, ready for sending
makeMsg :: BS.ByteString -> Message
makeMsg s = Message sender address text cmd
    where sender = BS.unpack $ case I.msg_prefix parsed of
                                 Just (I.NickName n _ _) -> n
                                 Just (I.Server n) -> n
                                 _ -> "none"
          address = (BS.unpack.headDef "".I.msg_params) parsed
          text = BS.unpack $ atDef "" (I.msg_params parsed) 1
          cmd = makeCommand $ BS.unpack $ I.msg_command parsed
          parsed = fromJust $ I.decode s

-- | Turns a string into an IRC command
makeCommand :: (Eq a, IsString a) => a -> Command
makeCommand s = case s of
                  "PRIVMSG" -> PRIVMSG
                  "MODE" -> MODE
                  "JOIN" -> JOIN
                  "PING" -> PING
                  _ -> OTHER
                  

-- | Takes a fully-filled in IRC message and writes it to a handle
irc :: I.Message -> IRC a ()
irc m = do
  h <- gets socket
  let s = BS.unpack $ I.encode m
  lift $ hPrintf h "%s\r\n" s
  lift $ log $ printf "wrote %s" s

-- | Runs a prebuilt bot
runBot :: BotState a -> IRC a ()
runBot bot = do
  let handle msg = fromMaybe (\x -> return ()) (M.lookup (determineMsgType msg) (_eventHandlers bot)) $ makeMsg msg
  irc $ (I.nick.BS.pack._nick) bot
  irc $ I.user (BS.pack $ _nick bot) "0" "*" "realname"
  irc $ (I.joinChan.BS.pack._channel) bot
  forever $ do
             h <- gets socket
             i <- lift $ hIsEOF h
             if i
             then lift $ exitWith ExitSuccess
             else return ()
             t <- lift $ hGetLine h
             let s = init t
             lift $ log $ printf "got %s" s -- Logs the received raw IRC messages
             if ping s
             then pong s
             else do
               lift $ log $ printf "parsed %s" $ (show.makeMsg.BS.pack) s -- Logs the parsed received IRC messages
               handle $ BS.pack s
    where
      forever a = a >> forever a
      clean = drop 1 . dropWhile (/= ':') . drop 1
      ping x = "PING :" `isPrefixOf` x
      pong x = irc $ I.pong $ BS.pack $ drop 6 x
  
-- | Puts a string into an IRC privmsg, ready to be sent
privmsg :: String -> Message
privmsg s = Message "" "" s PRIVMSG

-- | Ready-to-send quit command
quit :: Reason -> Message
quit r = Message "" "" "" QUIT

-- | Builds a bot using the given builder and runs the resulting bot
run :: BotBuilder a () -> IO ()
run botBuilder = do
  bot <- execStateT botBuilder defaultBot
  h <- Network.connectTo (_server bot) $ PortNumber (fromIntegral 6667)
  hSetBuffering h NoBuffering
  initLogger (_logfile bot) (_errlogfile bot)
  evalStateT (runBot bot) $ IRCState { socket = h, customState = (_custState bot) }
