module OSRIC.Main (main) where
import Safe
import System.Console.CmdArgs.Implicit hiding (args)
import EasyIRCBot hiding (logfile, channel, server, nick)
import OSRIC.Dice (rollDice)
import OSRIC.Voting
import OSRIC.DM
import OSRIC.State

data BotArgs = BotArgs { server :: String
                       , channel :: String
                       , admins :: String
                       , logfile :: String
                       , errorlogfile :: String
                       , nick :: String
                       } deriving (Show, Data, Typeable)

botArgs = BotArgs { server = "irc.freenode.net" &= help "Server to connect to" &= typ "SERVER"
                  , channel = "#hs-irc-osric-test" &= help "Channel to join on connecting to server" &= typ "CHANNEL"
                  , admins = "kaashif pizza" &= help "List of users to be considered admins" &= typ "NICKS"
                  , logfile = "osric-irc.log" &= help "File to log INFO messages to" &= typ "FILE"
                  , errorlogfile = "osric-irc-errors.log" &= help "File to log ERROR messages to" &= typ "FILE"
                  , nick = "osric-bot" &= help "The bot's name" &= typ "NICK"
                  } &= summary "irc-osric v0.0.1" &= program "irc-osric"


helptext = "Valid commands: quit, id, roll, vote, results, dm, help."

bot :: BotArgs -> BotBuilder OSRICState ()
bot a = do
  connectTo $ server a
  join $ channel a
  logInfoTo $ logfile a
  logErrorsTo $ errorlogfile a
  myNameIs $ nick a
  setInitialState initialOSRIC

  on PRIVMSG $ \msg -> do
    if "!" `isPrefixOf` (msg^.text)
    then if (msg^.address) == "osric-bot"
         then replyTo (msg^.sender) =<< (makeResponse msg)
         else replyTo (msg^.address) =<< (makeResponse msg)
    else return ()
        where makeResponse msg = if (msg^.sender) `elem` adm
                               then makeAdminResponse msg
                               else makeNormalResponse msg
              makeNormalResponse msg = case v msg of
                                       "quit" -> return $ quit NoReason
                                       "id" -> return $ privmsg $ intercalate " " $ ar msg
                                       "roll" -> lift $ fmap (privmsg.show) $
                                                 rollDice (readDef 1 $ atDef "1" (ar msg) 0)
                                                          (readDef 6 $ atDef "6" (ar msg) 1)
                                       "vote" -> (msg^.sender) `voteFor` (atDef "no-one" (ar msg) 0)
                                       "results" -> tellResults
                                       "dm" -> tellDM
                                       "help" -> return $ privmsg helptext
                                       _ -> return $ privmsg "Invalid command"
              makeAdminResponse msg = case v msg of
                                      "dm" -> case length $ ar msg of
                                                0 -> tellDM
                                                _ -> setDM $ (ar msg) !! 0
                                      _ -> makeNormalResponse msg
              adm = words $ admins a
              v m = verb $ p m
              ar m = args $ p m
              p m = parse (m^.text)

main :: IO ()
main = cmdArgs botArgs >>= (run.bot)
