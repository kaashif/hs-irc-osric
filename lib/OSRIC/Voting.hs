module OSRIC.Voting where
import EasyIRCBot
import OSRIC.State
import qualified Data.Map as M

voteFor :: String -> String -> IRC OSRICState Message
voteFor voter target = do
  s <- get
  let cs = concrete $ customState s
  put $ s { customState = addVote voter target cs }
  return $ privmsg $ "Got vote from " ++voter++" for "++target++"."

addVote :: String -> String -> OSRICState -> OSRICState
addVote voter target s = s { votes = M.insert voter target (votes s) }

tellResults :: IRC OSRICState Message
tellResults = do
  s <- gets (votes.customState)
  let m = intercalate ", " $
          map (\xs -> concat [show $ head xs, " - ", show $ length xs]) . group . sort $
          M.elems s
  return $ privmsg m
