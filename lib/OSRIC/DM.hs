module OSRIC.DM where
import EasyIRCBot
import OSRIC.State

setDM :: String -> IRC OSRICState Message
setDM n = do
  s <- get
  let cs = concrete $ customState s
  put $ s { customState = cs { dm = n } }
  return $ privmsg $ "Set DM to " ++ n ++"."

tellDM :: IRC OSRICState Message
tellDM = gets (dm.customState) >>= \d -> return $ privmsg $ "The DM is: "++d++"."
