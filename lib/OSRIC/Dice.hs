module OSRIC.Dice (rollDice) where
import System.Random
import Control.Monad

rollDice :: Int -> Int -> IO [Int]
rollDice x y = replicateM x $ rollDie y
    where rollDie y = randomRIO (1,y)
