module OSRIC.World where
import OSRIC.Character

data World = World { players :: [Character]
                   }

initialWorld :: World
initialWorld = World { players = []
                     }
