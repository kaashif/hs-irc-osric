module OSRIC.State where
import Data.Data
import Data.Typeable
import qualified Data.Map as M

data OSRICState = OSRICState { dm :: String
                             , votes :: M.Map String String
                             } deriving (Data, Typeable)

initialOSRIC = OSRICState { dm = "kaashif"
                          , votes = M.empty
                          }

-- | Converts the unknown data type to the expected `OSRICState`.
-- | It's OK that this is a partial function, because I will never
-- | put anything but a real `OSRICState` into it.
concrete :: Typeable a => a -> OSRICState
concrete x = case cast x of
               Just (x :: OSRICState) -> x
               Nothing -> error "invalid OSRICState"
