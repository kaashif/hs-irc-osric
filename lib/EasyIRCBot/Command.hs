module EasyIRCBot.Command where
import Safe

data Cmd = Cmd { verb :: String
               , args :: [String]
               }

parse :: String -> Cmd
parse s = Cmd { verb = tailDef "help" $ headDef "!help" ws
              , args = tailDef [] ws
              }
    where ws = words s
