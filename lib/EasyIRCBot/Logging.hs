module EasyIRCBot.Logging where
import System.Log.Logger
import System.Log.Handler.Simple
import System.Log.Handler (setFormatter)
import System.Log.Formatter

initLogger :: String -> String -> IO ()
initLogger logfile errlogfile = do
  h <- fileHandler logfile INFO >>= \lh -> return $
       setFormatter lh (simpleLogFormatter "[$time : $prio] $msg")
  e <- fileHandler errlogfile ERROR >>= \lh -> return $
       setFormatter lh (simpleLogFormatter "[$time : $prio] $msg")
  updateGlobalLogger "irc-osric" (setHandlers [h,e])
  updateGlobalLogger "irc-osric" $ setLevel INFO

log :: String -> IO ()
log msg = infoM "irc-osric" msg

err :: String -> IO ()
err msg = errorM "irc-osric" msg
