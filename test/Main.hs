module Main where
import Test.HUnit
import EasyIRCBot

privmsgValid = TestCase (assertEqual "privmsg valid"
                         (Message "" "" "test" PRIVMSG)
                         (privmsg "test"))

main = runTestTT $ TestList [TestLabel "privmsg valid" privmsgValid]
